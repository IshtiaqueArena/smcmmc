package com.smcmms.smc_mmc.viewmodel;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import com.google.gson.JsonObject;
import com.smcmms.smc_mmc.LoginActivity;
import com.smcmms.smc_mmc.repository.LoginRepository;

import java.util.ArrayList;
import java.util.HashMap;

public class LoginViewModel extends AndroidViewModel {

    private LoginRepository loginRepository;
    public LoginViewModel(@NonNull Application application) {
        super(application);
    }
    public MutableLiveData<ArrayList<HashMap<String, String>>> getStatus(){
        return loginRepository.getLoginStatus();
    }

    public void initialize(LoginActivity application, JsonObject reqJsonObj){
        Log.e("init--->", "initialize: "+reqJsonObj.toString() );
        loginRepository = new LoginRepository(application,reqJsonObj);

    }
}
