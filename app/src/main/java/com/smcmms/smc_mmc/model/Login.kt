package com.smcmms.smc_mmc.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Login {
    @SerializedName("user_info")
    @Expose
    var userInfo: List<UserInfo>? = null


    class UserInfo {
        @SerializedName("status")
        @Expose
        var status: Int? = null

        @SerializedName("message")
        @Expose
        var message: String? = null

        @SerializedName("data")
        @Expose
        var data: Data? = null

        class Data {
            @SerializedName("id")
            @Expose
            var id: String? = null

            @SerializedName("name")
            @Expose
            var name: String? = null

            @SerializedName("username")
            @Expose
            var username: String? = null

            @SerializedName("group_id")
            @Expose
            var groupId: String? = null

        }

    }

}