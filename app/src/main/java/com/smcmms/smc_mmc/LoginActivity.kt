package com.smcmms.smc_mmc

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.gson.JsonObject
import com.smcmms.smc_mmc.viewmodel.LoginViewModel
import org.json.JSONException

class LoginActivity : AppCompatActivity() {

    var mobileno : EditText? = null
    var password : EditText? = null
    var submitBtn: Button?=null
    var loginViewModel: LoginViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        actionBar?.hide()
        setContentView(R.layout.activity_login)

        submitBtn = findViewById<Button>(R.id.loginBtn)
        submitBtn?.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view: View?) {

                mobileno = findViewById(R.id.useridEdt) as EditText
                password = findViewById(R.id.passwordEdt) as EditText

                doLogin(mobileno?.text.toString(), password?.text.toString())
            }
        });
    }

    private fun doLogin(user: String, password: String) {

        val reqJsonObj = JsonObject()
        try {
            reqJsonObj.addProperty("username", user.trim { it <= ' ' })
            reqJsonObj.addProperty("password", password.trim { it <= ' ' })
            Log.e("reqjson", reqJsonObj.toString())
        } catch (e: JSONException) {
            e.printStackTrace()
            Log.e("rerror", e.printStackTrace().toString());
        }
        loginViewModel = ViewModelProvider.AndroidViewModelFactory.getInstance(application).create(LoginViewModel::class.java)
        loginViewModel!!.initialize(this@LoginActivity, reqJsonObj)
        loginViewModel!!.status.observe(this@LoginActivity, Observer { stringStringHashMap ->
            var success = stringStringHashMap.get(0).get("success");
            var message = stringStringHashMap.get(0).get("message");

            if(success == "1"){
                Toast.makeText(applicationContext, ""+message, Toast.LENGTH_SHORT).show()
            }
            Log.e("suceess",success.toString())

        });
    }
}