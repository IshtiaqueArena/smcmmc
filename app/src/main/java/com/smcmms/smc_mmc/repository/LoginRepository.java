package com.smcmms.smc_mmc.repository;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.smcmms.smc_mmc.LoginActivity;
import com.smcmms.smc_mmc.model.Login;
import com.smcmms.utils.Helper;
import com.smcmms.utils.RetrofitInstance;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginRepository {

    JsonObject reqJsonObj;


    Context context;
    public LoginRepository(LoginActivity application, JsonObject reqJsonObj) {
        this.reqJsonObj = reqJsonObj;
        context = application;
        Log.e("json req--->", "RegistrationRepository: "+reqJsonObj.size() );

    }

    MutableLiveData<ArrayList<HashMap<String, String>>> success;

    public MutableLiveData<ArrayList<HashMap<String, String>>> getLoginStatus(){

        Log.e("RESPO____>","BAL ");
        if (success == null) {
            success = new MutableLiveData<ArrayList<HashMap<String, String>>>();
        }

        //RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"),reqJsonObj.toString());

        Call<Login> call = RetrofitInstance.getInstance().getApi().doLogin(reqJsonObj);
        Helper.showLoader(context,"");
        call.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {

                Log.e("response",response.body().toString());

                // length.setValue(response.body().length());
                try {

                    ArrayList<HashMap<String, String>> responselist = new ArrayList<>();
                    HashMap<String, String> responseMap = new HashMap<>();

                    responseMap.put("success", String.valueOf(response.body().getUserInfo().get(0).getStatus()));
                    responseMap.put("message", String.valueOf(response.body().getUserInfo().get(0).getStatus()));

                    responselist.add(responseMap);

                    success.setValue(responselist);
                    Helper.cancelLoader();
                }
                catch (JsonIOException e) {
                    e.printStackTrace();
                    Log.e("RESPO____>","BAL "+e.getMessage());
                }

            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {

            }
        });

        return success;



    }

}
