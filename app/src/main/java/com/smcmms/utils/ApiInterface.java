package com.smcmms.utils;

import com.google.gson.JsonObject;
import com.smcmms.smc_mmc.model.Login;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface ApiInterface {

    @POST("user_login.json")
    Call<Login> doLogin(@Body JsonObject jsonObject);


}
